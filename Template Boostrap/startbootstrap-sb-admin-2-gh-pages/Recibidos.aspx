﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Recibidos.aspx.cs" Inherits="EnviadorSMS.Recibidos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <div>
        <h1 class="h3 mb-4 text-gray-800">Panel de Recibidos</h1>
    </div>

    <asp:Label Text="panel de recibidos!" runat="server" />--%>

<!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Panel de Recibidos
    </h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Registros</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                          
                    <asp:UpdatePanel ID="upGridViewSMSRecibidos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <asp:GridView ID="gvSMSRecibidos" CssClass="table table-striped table-bordered table-hover" data-model="SMS.aspx"
                                Width="100%" AutoGenerateColumns="False" runat="server" 
                                EmptyDataText="Sin datos para mostrar."
                                OnPageIndexChanging="GridViewRecibidos_PageIndexChanging" PageSize="25" AllowPaging="True"
                                >
                                <Columns>
                                    <asp:BoundField HeaderText="#" DataField="RecibidoIdRecibido" />
                                    <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                    <asp:BoundField HeaderText="Celular" DataField="RecibidoCelular" />
                                    <asp:BoundField HeaderText="Mensaje" DataField="RecibidoMensaje" />
                                    <asp:BoundField HeaderText="Version Soft" DataField="VersionNombre" />
                                    <asp:BoundField HeaderText="Terminal" DataField="TerminalDescripcion" />
                                    <asp:BoundField HeaderText="Fecha de Alta" DataField="RecibidoFechaAlta" ItemStyle-CssClass="date-time" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>

              </div>
            </div>
          </div>
</asp:Content>
