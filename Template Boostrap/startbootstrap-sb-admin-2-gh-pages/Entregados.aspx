﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Entregados.aspx.cs" Inherits="EnviadorSMS.Entregados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
          <h1 class="h3 mb-2 text-gray-800">Panel de Entregados
    </h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Registros</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">


                          
                    <asp:UpdatePanel ID="upGridViewSMSEntregados" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <asp:GridView ID="gvSMSEntregados" CssClass="table table-striped table-bordered table-hover" data-model="SMS.aspx"
                                Width="100%" AutoGenerateColumns="False" runat="server" 
                                EmptyDataText="Sin datos para mostrar."
                                AllowPaging="True"
                                OnPageIndexChanging="gvSMSEntregados_PageIndexChanging" PageSize="25"
                                >
                                <Columns>
                                    <asp:BoundField HeaderText="# Entregado" DataField="EntregadoId" />
                                    <asp:BoundField HeaderText="Celular" DataField="EntregadoCelular" />
                                    <asp:BoundField HeaderText="Fecha Entregado" DataField="EntregadoFHAlta" ItemStyle-CssClass="date-time" />
                                    <asp:BoundField HeaderText="Terminal Entregado" DataField="EntregadoTerminalDescripcion" />
                                    <asp:BoundField HeaderText="Entregado Soft Version" DataField="EntregadoVersion" />
                                    <asp:BoundField HeaderText="# Discado" DataField="DiscadoId" />
                                    <asp:BoundField HeaderText="Fecha Creado" DataField="FHCreado" ItemStyle-CssClass="date-time" />
                                    <asp:BoundField HeaderText="Fecha Discado" DataField="FHDiscado" ItemStyle-CssClass="date-time" />
                                    <asp:BoundField HeaderText="Terminal Discado" DataField="DiscadoTerminalDescripcion" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>

              </div>
            </div>
          </div>
</asp:Content>
