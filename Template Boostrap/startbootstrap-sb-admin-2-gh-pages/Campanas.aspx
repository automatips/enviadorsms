﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Campanas.aspx.cs" Inherits="EnviadorSMS.Campanas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
          <h1 class="h3 mb-2 text-gray-800">Panel de Campañas
    </h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Registros</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                          
                    <asp:UpdatePanel ID="upGridViewCampanas" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <asp:GridView ID="gvCampanas" CssClass="table table-striped table-bordered table-hover" data-model="SMS.aspx"
                                Width="100%" AutoGenerateColumns="False" runat="server" 
                                EmptyDataText="Sin datos para mostrar."
                                OnPageIndexChanging="GridViewCampanas_PageIndexChanging" PageSize="25" AllowPaging="True"
                                >
                                <Columns>
                                    <asp:BoundField HeaderText="#" DataField="IdCampana" />
                                    <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>
                                    <asp:BoundField HeaderText="Nombre" DataField="Nombre" />
                                    <asp:BoundField HeaderText="Speech" DataField="SpeechSMS" />
                                    <asp:BoundField HeaderText="Caracteres" DataField="Caracteres" />
                                    <asp:BoundField HeaderText="Es Modulo Automatico" DataField="EsModuloAutomatico" /> 
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>

              </div>
            </div>
          </div>
</asp:Content>
