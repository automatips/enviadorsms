﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using GsmComm.PduConverter;
using GsmComm.PduConverter.SmartMessaging;
using GsmComm.GsmCommunication;
using GsmComm.Interfaces;
using GsmComm.Server;
using System.Globalization;
using System.IO.Ports;

namespace GSMModem
{
    public partial class Form1 : Form
    {
        private GsmCommMain comm;
        private delegate void SetTextCallback(string text);

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                //cmbCOM.Items.Add(port);
                leersms(6);
            }
        }

        TextBox txtMessage = new TextBox();
        TextBox txtDonorName = new TextBox();
        TextBox txtNumber = new TextBox();
        TextBox txtDonorId = new TextBox();
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btnSend_Click(object sender, EventArgs e)
        {
            Operation code = new Operation();
            try
            {
                SmsSubmitPdu pdu;
                byte dcs = (byte)DataCodingScheme.GeneralCoding.Alpha7BitDefault;
                pdu = new SmsSubmitPdu("dale que son las "+DateTime.Now.ToString(), Convert.ToString("3516979510"), dcs);
                int times = 1;
                for (int i = 0; i < times; i++)
                {
                    comm.SendMessage(pdu);
                }
            }
            catch
            {
                MessageBox.Show("SMS not send");
            }

        }



        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (cmbCOM.Text == "")
            {
                MessageBox.Show("Invalid Port Name");
                return;
            }
             comm = new GsmCommMain(cmbCOM.Text , 9600, 150);
            Cursor.Current = Cursors.Default;

            bool retry;
            do
            {
                retry = false;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    comm.Open();
                    
                    Cursor.Current = Cursors.Default;

                    
                    MessageBox.Show("Modem Connected Sucessfully");
                }
                catch (Exception)
                {
                    Cursor.Current = Cursors.Default;
                    if (MessageBox.Show(this, "GSM Modem is not available", "Check",
                        MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning) == DialogResult.Retry)
                        retry = true;
                    else
                   {

                        //Close();
                        return;
                    }
               }
            }
            while (retry);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            leersms(6);
        }

        /// <summary>
        /// ///////////////////////////////////////////////////////////
        /// </summary>
        string actualtext;
        public void leersms(int puerto)
        {
            try
            {
                InitializeComponent();
                comm = new GsmCommMain(puerto, 9600); //115200
                comm.Open();

                comm.EnableMessageNotifications();
                comm.MessageReceived += new MessageReceivedEventHandler(comm_MessageReceived);
                var mensajesTelefono = comm.ReadMessages(PhoneMessageStatus.All, PhoneStorageType.Phone);
                var mensajesSim = comm.ReadMessages(PhoneMessageStatus.All, PhoneStorageType.Sim);

                foreach (var m in mensajesTelefono)
                {
                    leerMensaje(m);
                    ;
                }

                foreach (var m in mensajesSim)
                {
                    leerMensaje(m);
                }

                ////Eliminar todo del aparato
                //comm.DeleteMessages(DeleteScope.All, PhoneStorageType.Phone);

                ////Eliminar todo del sim
                //comm.DeleteMessages(DeleteScope.All, PhoneStorageType.Sim);
            }
            catch
            {
            }
        }

        public void leerMensaje(DecodedShortMessage men)
        {
            MessageBox.Show(
                        "Direccion: " + men.Data.SmscAddress.ToString() +
                        ". : " + men.Data +
                        ". Mensaje" + men.Data.UserDataText
                        );
        }

        private void comm_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            IMessageIndicationObject obj = e.IndicationObject;
            MemoryLocation loc = (MemoryLocation)obj;
            DecodedShortMessage[] messages;
            messages = comm.ReadMessages(PhoneMessageStatus.All, PhoneStorageType.Sim);

            foreach (DecodedShortMessage message in messages)
            {
                SmsDeliverPdu data = new SmsDeliverPdu();

                SmsPdu smsrec = message.Data;
                ShowMessage(smsrec);
            }
        }

        private void ShowMessage(SmsPdu pdu)
        {// Received message
            SmsDeliverPdu data = (SmsDeliverPdu)pdu;
            Console.WriteLine();

            actualtext = data.UserDataText;
            string celular = data.OriginatingAddress;
            MessageBox.Show(celular+" "+actualtext);
            SetSomeText(actualtext);

            return;
        }
        
        private void SetSomeText(string text)
        {
            SetTextCallback d = new SetTextCallback(SetSomeText);
            this.Invoke(d, new object[] { text });
        }
    }
}

