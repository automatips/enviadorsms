class ObjSMS:
    tipo = None
    inbox_id = None
    cabecera = ""
    cuerpo = ""
    estado = ""
    origen = ""
    wtf = ""
    fecha = ""
    hora = ""

    def __init__(self,id,_pdu,_tipo):
        self.inbox_id = id
        self.cuerpo = _pdu.encode('utf-16', 'surrogatepass').decode('utf-16')
        self.tipo = _tipo
        #pdu: {'smsc': '+54079000801', 'tpdu_length': 24, 'type': 'SMS-STATUS-REPORT', 'reference': 90, 'number': '3516863294', 'time': datetime.datetime(2019, 4, 1, 18, 49, 10, tzinfo=<gsmmodem.pdu.SmsPduTzInfo object at 0x0000000002143588>), 'discharge': datetime.datetime(2019, 4, 1, 18, 49, 10, tzinfo=<gsmmodem.pdu.SmsPduTzInfo object at 0x00000000021436D8>), 'status': 0}
