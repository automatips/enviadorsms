import os
from time import sleep

from Config import PID_FILE_FOLDER
from Main import BuscarModems


def main():
    while True:
        sleep(3)
        usb_modems = BuscarModems()
        for modem in usb_modems:
            pid_file = os.path.join(PID_FILE_FOLDER + "/" + modem.replace("COM", "COM_"))
            if os.path.isfile(pid_file):
                andando = True
                #puerto ocupado
                continue
            else:
                #iniciar
                command = "start python Main.py "+modem
                os.system(command)
                sleep(1)

if __name__ == '__main__':
    main()
