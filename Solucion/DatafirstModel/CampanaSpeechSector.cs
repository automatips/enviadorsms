﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace DatafirstModel
{
    public partial class CampanaSpeechSector : ISUD<CampanaSpeechSector>
    {
        public CampanaSpeechSector() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdCampanaSpeechSector { get; set; }

        public string Descripcion { get; set; }

        public int? IdCampana { get; set; }

        public int? IdSector { get; set; }

        public DateTime? FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}