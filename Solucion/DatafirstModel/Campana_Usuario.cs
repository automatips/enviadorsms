﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace DatafirstModel
{
    public partial class Campana_Usuario : ISUD<Campana_Usuario>
    {
        public Campana_Usuario() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdCampana_Usuario { get; set; }

        public int? IdCampana { get; set; }

        public int? IdUsuario { get; set; }

        public DateTime? FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }
    }
}