﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace DatafirstModel
{
    public partial class Numeracion : ISUD<Numeracion>
    {
        public Numeracion() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdNumeracion { get; set; }

        public int? DDN { get; set; }

        public long? DESDE { get; set; }

        public long? HASTA { get; set; }

        public string Operador { get; set; }

        public string Localidad { get; set; }

        public long? GENERADO { get; set; }

        public DateTime? FHUltimaGeneracion { get; set; }
    }
}