﻿// Created for MagicSQL using MagicMaker [v.3.51.119.1109]

using System;
using MagicSQL;

namespace DatafirstModel
{
    public partial class Sim : ISUD<Sim>
    {
        public Sim() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdSim { get; set; }

        public string NroSerie { get; set; }

        public string NroLinea { get; set; }

        public DateTime? FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }

        public DateTime? FHVencimientoSMS { get; set; }

        public int? CantidadSMS { get; set; }
    }
}