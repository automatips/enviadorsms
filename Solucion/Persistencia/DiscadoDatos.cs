﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace Persistencia
{
    public class DiscadoDatos
    {
        SqlConnection cnx;
        Conexion MiConexi = new Conexion();
        SqlCommand cmd = new SqlCommand();
        bool vexito;
        public DiscadoDatos()
        {
            cnx = new SqlConnection(MiConexi.GetConex());
        }
        #region WEBSERVICE - MODEMS TERMINALES
        public DataTable ObtenerNumeroDiscado(
            string pIMEI,
            string pNroSerieSim
            )
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ObtenerDiscado";

                cmd.Parameters.Add(new SqlParameter("@pIMEI", SqlDbType.VarChar, 50));
                cmd.Parameters["@pIMEI"].Value = pIMEI;

                cmd.Parameters.Add(new SqlParameter("@pNroSerieSim", SqlDbType.VarChar, 50));
                cmd.Parameters["@pNroSerieSim"].Value = pNroSerieSim;

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Discado");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Discado"]);
        }

        public bool InsertarRecibido(
            string pCelular, 
            string pMensaje,
            string pIMEI,
            string pNroSerieSim
            )
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_InsertarRecibido";
            try
            {

                cmd.Parameters.Add(new SqlParameter("@pCelular", SqlDbType.VarChar, 50));
                cmd.Parameters["@pCelular"].Value = pCelular;

                cmd.Parameters.Add(new SqlParameter("@pMensaje", SqlDbType.VarChar, 500));
                cmd.Parameters["@pMensaje"].Value = pMensaje;

                cmd.Parameters.Add(new SqlParameter("@pIMEI", SqlDbType.VarChar, 50));
                cmd.Parameters["@pIMEI"].Value = pIMEI;

                cmd.Parameters.Add(new SqlParameter("@pNroSerieSim", SqlDbType.VarChar, 50));
                cmd.Parameters["@pNroSerieSim"].Value = pNroSerieSim;

                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }

        public bool InsertarEntregado(string pIMEI, string pCelular)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_InsertarEntregado";
            try
            {

                cmd.Parameters.Add(new SqlParameter("@pIMEI", SqlDbType.VarChar, 50));
                cmd.Parameters["@pIMEI"].Value = pIMEI;

                cmd.Parameters.Add(new SqlParameter("@pCelular", SqlDbType.VarChar, 50));
                cmd.Parameters["@pCelular"].Value = pCelular;

                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }
        #endregion

        #region PAGINA WEB      
        public bool RegistroUsuario(
                string pNombre,
                string pUsuario,
                string pPassword,
                int pIdNivel,
                string pEmail,
                string pCelular,
                string pLocalidad
                )
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_RegistroUsuario";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@pNombre", SqlDbType.VarChar, 50));
                cmd.Parameters["@pNombre"].Value = pNombre;
                cmd.Parameters.Add(new SqlParameter("@pUsuario", SqlDbType.VarChar, 50));
                cmd.Parameters["@pUsuario"].Value = pUsuario;
                cmd.Parameters.Add(new SqlParameter("@pPassword", SqlDbType.VarChar, 50));
                cmd.Parameters["@pPassword"].Value = pPassword;
                cmd.Parameters.Add(new SqlParameter("@pIdNivel", SqlDbType.Int));
                cmd.Parameters["@pIdNivel"].Value = pIdNivel;
                cmd.Parameters.Add(new SqlParameter("@pEmail", SqlDbType.VarChar, 100));
                cmd.Parameters["@pEmail"].Value = pEmail;
                cmd.Parameters.Add(new SqlParameter("@pCelular", SqlDbType.VarChar, 50));
                cmd.Parameters["@pCelular"].Value = pCelular;
                cmd.Parameters.Add(new SqlParameter("@pLocalidad", SqlDbType.VarChar, 100));
                cmd.Parameters["@pLocalidad"].Value = pLocalidad;
                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }

        public DataTable ObtenerRecibidos(int pIdUsuario)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ObtenerRecibidos";

                SqlDataAdapter miada;

                cmd.Parameters.Add(new SqlParameter("@pIdUsuario", SqlDbType.Int));
                cmd.Parameters["@pIdUsuario"].Value = pIdUsuario;

                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Recibido");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Recibido"]);
        }

        public DataTable ObtenerEntregados(int pIdUsuario)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ObtenerEntregados";

                SqlDataAdapter miada;

                cmd.Parameters.Add(new SqlParameter("@pIdUsuario", SqlDbType.Int));
                cmd.Parameters["@pIdUsuario"].Value = pIdUsuario;

                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Entregado");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Entregado"]);
        }

        public DataTable ObtenerDiscados(int pIdUsuario)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ObtenerDiscados";

                SqlDataAdapter miada;

                cmd.Parameters.Add(new SqlParameter("@pIdUsuario", SqlDbType.Int));
                cmd.Parameters["@pIdUsuario"].Value = pIdUsuario;

                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "DiscadoHistorico");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["DiscadoHistorico"]);
        }

        public DataTable ObtenerTerminales()
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ObtenerTerminales";

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Terminal");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Terminal"]);
        }

        public DataTable ObtenerCampanas(int pIdUsuario)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ObtenerCampanas";

                SqlDataAdapter miada;

                cmd.Parameters.Add(new SqlParameter("@pIdUsuario", SqlDbType.Int));
                cmd.Parameters["@pIdUsuario"].Value = pIdUsuario;

                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Campana");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Campana"]);
        }

        public DataTable ObtenerRecibidoEstados()
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ObtenerRecibidoEstados";

                SqlDataAdapter miada;
                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "RecibidoEstado");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["RecibidoEstado"]);
        }

        public DataTable ObtenerReporteUltimaHora(int pIdUsuario)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ReporteUltimaHora";

                SqlDataAdapter miada;

                cmd.Parameters.Add(new SqlParameter("@pIdUsuario", SqlDbType.Int));
                cmd.Parameters["@pIdUsuario"].Value = pIdUsuario;

                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Reporte");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Reporte"]);
        }

        public DataTable ObtenerRecibidoContactados(int pIdUsuario)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_ObtenerRecibidoContactados";

                SqlDataAdapter miada;

                cmd.Parameters.Add(new SqlParameter("@pIdUsuario", SqlDbType.Int));
                cmd.Parameters["@pIdUsuario"].Value = pIdUsuario;


                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "RecibidoContactado");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["RecibidoContactado"]);
        }

        public DataTable Login(string pEmail, string pPassword)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_LoginUsuario";

                SqlDataAdapter miada;

                cmd.Parameters.Add(new SqlParameter("@pEmail", SqlDbType.VarChar,50));
                cmd.Parameters["@pEmail"].Value = pEmail;

                cmd.Parameters.Add(new SqlParameter("@pPassword", SqlDbType.VarChar, 50));
                cmd.Parameters["@pPassword"].Value = pPassword;

                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Usuario");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Usuario"]);
        }

        public DataTable InsertarDiscadoManual(int pIdCampana, string pCelular)
        {
            DataSet dts = new DataSet();
            try
            {
                cmd.Connection = cnx;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PRC_InsertarDiscadoManual";

                SqlDataAdapter miada;

                cmd.Parameters.Add(new SqlParameter("@pIdCampana", SqlDbType.Int));
                cmd.Parameters["@pIdCampana"].Value = pIdCampana;

                cmd.Parameters.Add(new SqlParameter("@pCelular", SqlDbType.VarChar, 50));
                cmd.Parameters["@pCelular"].Value = pCelular;

                miada = new SqlDataAdapter(cmd);
                miada.Fill(dts, "Discado");
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmd.Parameters.Clear();
            }
            return (dts.Tables["Discado"]);
        }

        public bool CambiarRecibidoEstado(int IdRecibidoEstado, string pCelular)
        {
            cmd.Connection = cnx;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRC_CambiarRecibidoEstado";
            try
            {
                cmd.Parameters.Add(new SqlParameter("@vIdRecibidoEstado", SqlDbType.Int));
                cmd.Parameters["@vIdRecibidoEstado"].Value = IdRecibidoEstado;

                cmd.Parameters.Add(new SqlParameter("@pCelular", SqlDbType.VarChar, 50));
                cmd.Parameters["@pCelular"].Value = pCelular;

                cnx.Open();
                cmd.ExecuteNonQuery();
                vexito = true;
            }
            catch (SqlException)
            {
                vexito = false;
            }
            finally
            {
                if (cnx.State == ConnectionState.Open)
                {
                    cnx.Close();
                }
                cmd.Parameters.Clear();
            }
            return vexito;
        }
        #endregion
    }
}