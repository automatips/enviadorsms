using System;


namespace Modelo
{
    public class Error
    {
        private int id;
        private string nombre, descripcion;
        private DateTime fhAlta, fhBaja;
        private int estado;


        public int idError
        {
            get { return id; }
            set { id = value; }
        }


        public string nombreError
        {
            get { return nombre; }
            set { nombre = value; }
        }


        public string descripcionError
        {
            get { return descripcion; }
            set { descripcion = value; }
        }


        public DateTime fhAltaError
        {
            get { return fhAlta; }
            set { fhAlta = value; }
        }


        public DateTime fhBajaError
        {
            get { return fhBaja; }
            set { fhBaja = value; }
        }


        public int estadoError
        {
            get { return estado; }
            set { estado = value; }
        }


    }
}
