﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Persistencia;
using System.Data;

namespace EnviadorSMS
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
        }

        private void IniciarSesion()
        {
            //Session.Add("nivelAdmin","true");
            Session.Remove("nivelAdmin");
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                DiscadoDatos persistencia = new DiscadoDatos();
                DataTable dtLogin = persistencia.Login(txtEmail.Text, txtPassword.Text);
                if (dtLogin.Rows.Count > 0)
                {
                    foreach (DataRow dtRow in dtLogin.Rows)
                    {
                        Session.Add("usuIdUsuario", dtRow["IdUsuario"].ToString());
                        Session.Add("usuNombre", dtRow["Nombre"].ToString());
                        Session.Add("usuUsuario", dtRow["Usuario"].ToString());
                        Session.Add("usuIdNivel", dtRow["IdNivel"].ToString());
                        Session.Add("usuIdNivel", dtRow["FHAlta"].ToString());
                        Session.Add("usuEmail", dtRow["Email"].ToString());
                        Session.Add("usCelular", dtRow["Celular"].ToString());
                        Session.Add("usuLocalidad", dtRow["Localidad"].ToString());
                        UCTraza1.InsertarTraza("Ingreso Exitoso - Email: " + txtEmail.Text + " Password: " + txtPassword.Text);
                        Response.Redirect("Entregados.aspx");
                    }
                }
                else
                {
                    UCTraza1.InsertarTraza("Ingreso Fallido -  Email: " + txtEmail.Text + " Password: " + txtPassword.Text);
                }
            }
            catch (Exception ex)
            {
                UCTraza1.InsertarTraza("Error en login: "+ex.Message+ ".  Email: " + txtEmail.Text + " Password: " + txtPassword.Text);
            }
        }
    }
}