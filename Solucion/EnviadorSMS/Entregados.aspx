﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Entregados.aspx.cs" Inherits="EnviadorSMS.Entregados" %>
<%@ Register Src="~/Controls/UCTraza.ascx" TagPrefix="uc" TagName="UCTraza" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
          <h1 class="h3 mb-2 text-gray-800">SMS Enviados
    </h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Registros</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                          
                    <asp:UpdatePanel ID="upGridViewSMSEntregados" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>

                            <asp:GridView ID="gvSMSEntregados" CssClass="table table-striped table-bordered table-hover" data-model="SMS.aspx"
                                Width="100%" AutoGenerateColumns="False" runat="server" 
                                EmptyDataText="Sin datos para mostrar."
                                AllowPaging="True"
                                OnPageIndexChanging="gvSMSEntregados_PageIndexChanging" PageSize="100"
                                >
                                <Columns>
                                    <asp:BoundField HeaderText="# Enviado" DataField="EntregadoId" />
                                    <asp:BoundField HeaderText="Celular" DataField="EntregadoCelular" />
                                    <asp:BoundField HeaderText="FH Entregado" DataField="EntregadoFHAlta" ItemStyle-CssClass="date-time" />
                                    <asp:BoundField HeaderText="Campaña" DataField="CampanaNombre" />
                                    <asp:BoundField HeaderText="Speech" DataField="Speech" />
                                </Columns>
                            </asp:GridView>

                            <asp:GridView ID="gvSMSEntregadosUser" CssClass="table table-striped table-bordered table-hover" data-model="SMS.aspx"
                                Width="100%" AutoGenerateColumns="False" runat="server" 
                                EmptyDataText="Sin datos para mostrar."
                                AllowPaging="True"
                                OnPageIndexChanging="gvSMSEntregados_PageIndexChanging" PageSize="100"
                                >
                                <Columns>
                                    <asp:BoundField HeaderText="# Entregado" DataField="EntregadoId" />
                                    <asp:BoundField HeaderText="Celular" DataField="EntregadoCelular" />
                                    <asp:BoundField HeaderText="FH Entregado" DataField="EntregadoFHAlta" ItemStyle-CssClass="date-time" />
                                    <asp:BoundField HeaderText="FH Enviado" DataField="FHDiscado" ItemStyle-CssClass="date-time" />
                                    <asp:BoundField HeaderText="Speech" DataField="Speech" />
                                </Columns>
                            </asp:GridView>

                        </ContentTemplate>
                    </asp:UpdatePanel>

              </div>
            </div>
          </div>
     <uc:UCTraza runat="server" ID="UCTraza1" />
</asp:Content>
