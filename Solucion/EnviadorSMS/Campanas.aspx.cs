﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Persistencia;
using Modelo;
using System.Data;

namespace EnviadorSMS
{
    public partial class Campanas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarCampanas();
            comboTestingManual();
        }

        protected void comboTestingManual()
        {
            foreach (GridViewRow gvRow in gvCampanas.Rows)
            {
                string vIdCampana = gvRow.Cells[0].Text;
                string vNombreCampana = gvRow.Cells[1].Text;

                ListItem li = new ListItem();
                li.Value = vIdCampana;
                li.Text = vNombreCampana;

                ddlCampaña.Items.Add(li);
            }

            if (ddlCampaña.Items.Count > 1)
            {
                ddlCampaña.Enabled = true;
            }

            if (ddlCampaña.Items.Count > 0)
            {
                btnEnvio.Enabled = true;
            }
            upTestingManual.Update();
        }

        protected void cargarCampanas()
        {
            if (Session["usuIdUsuario"] != null)
            {
                try
                {
                    UCTraza1.InsertarTraza("Obteniendo entregados con IdUsuario: " + Session["usuIdUsuario"].ToString());

                    DiscadoDatos discadoDatos = new DiscadoDatos();
                    gvCampanas.DataSource = discadoDatos.ObtenerCampanas(Convert.ToInt32(Session["usuIdUsuario"].ToString()));
                    gvCampanas.DataBind();
                    upGridViewCampanas.Update();                    
                }
                catch (Exception)
                {
                    Response.Redirect("Precios.aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void GridViewCampanas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvCampanas.PageIndex = e.NewPageIndex;
                gvCampanas.DataBind();
                cargarCampanas();
            }
            catch (Exception)
            {
            }
        }

        protected void btnEnvio_Click(object sender, EventArgs e)
        {
            string vCelular = txtCelularTesting.Text;
            int vIdCampana = Convert.ToInt32(ddlCampaña.SelectedValue);
            ErrorHandler eh = new ErrorHandler();

            int vCelularInt = 0;
            try
            {
                vCelularInt = Convert.ToInt32(vCelular);
            }
            catch { }

            if (vCelular.Length != 10)
            {
                eh.Mensaje = "El largo del teléfono celular debe ser igual a 10. Por ej: '3516099788', siendo '0351' el código de área y '156099788' el nro de línea";
                eh.Descripcion = "Por favor reintente nuevamente";
            }

            if (vCelular == string.Empty)
            {
                eh.Mensaje = "Debe ingresar el celular destinatario de la prueba";
            }

            if (eh.Mensaje == null)
            {
                try
                {
                    DiscadoDatos insertDiscadoManual = new DiscadoDatos();
                    DataTable dt = insertDiscadoManual.InsertarDiscadoManual(vIdCampana, vCelular);

                    eh.Mensaje = "EXITO";
                    btnEnvio.Text = "Enviar otro SMS";
                    eh.Descripcion = "Se ha enviado el SMS a " + txtCelularTesting.Text + ". " + " Podrás visualizarlo en 'Bandeja de Salida', y en 'SMS Enviados' cuando sea recibido por el destinatario";
                    txtCelularTesting.Text = "";
                }
                catch(Exception ex)
                {
                    eh.Mensaje = "Error al intentar enviar el SMS";
                    eh.Descripcion = ex.Message;
                }
            }
            upTestingManual.Update();
            mensaje(eh);
        }

        protected void mensaje(ErrorHandler eh)
        {
            if (eh.Mensaje == "EXITO")
            {
                lblErrorHandler.Text = "Acción realizada correctamente! "+". "+eh.Descripcion+". " + DateTime.Now.ToString();
                lblErrorHandler.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblErrorHandler.Text = eh.Mensaje+". "+eh.Descripcion+". " + DateTime.Now.ToString();
                lblErrorHandler.ForeColor = System.Drawing.Color.Red;                
            }
            upErrorHandler.Update();
        }
    }
}