﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Persistencia;
using System.Data;

namespace EnviadorSMS
{
    public partial class Recibidos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarRecibidos();
        }

        protected void cargarRecibidos()
        {
            if (Session["usuIdUsuario"] != null)
            {
                try
                {
                    UCTraza1.InsertarTraza("Obteniendo Recibidos IdUsuario: "+ Session["usuIdUsuario"].ToString());
                    DiscadoDatos discadoDatos = new DiscadoDatos();
                    DataTable dtRecibidos = discadoDatos.ObtenerRecibidos(Convert.ToInt32(Session["usuIdUsuario"].ToString()));
                    gvSMSRecibidos.DataSource = dtRecibidos;
                    gvSMSRecibidos.DataBind();
                    upGridViewSMSRecibidos.Update();
                }
                catch (Exception ex)
                {
                    UCTraza1.InsertarTraza("Error al recuperar recibidos: "+ex.Message);
                }
                
            }
            else
            {
                Response.Redirect("Precios.aspx");
            }            
        }

        protected DataTable obtenerEstados()
        {
            DataTable dt = new DataTable();
            DiscadoDatos discadoDatos = new DiscadoDatos();
            dt = discadoDatos.ObtenerRecibidoEstados();
            return dt;
        }

        protected void gvSMSRecibidos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSMSRecibidos.PageIndex = e.NewPageIndex;
                gvSMSRecibidos.DataBind();
                cargarRecibidos();
            }
            catch (Exception)
            {
            }
        }

        protected void gvSMSRecibidos_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            string vCelular = gvSMSRecibidos.Rows[e.NewSelectedIndex].Cells[1].Text;

            DiscadoDatos di = new DiscadoDatos();
            di.CambiarRecibidoEstado(2, vCelular);

            cargarRecibidos();
        }

        protected void ddlEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarRecibidos();
        }

        protected void gvSMSRecibidos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string vCelular = gvSMSRecibidos.Rows[e.RowIndex].Cells[1].Text;

            DiscadoDatos di = new DiscadoDatos();
            di.CambiarRecibidoEstado(3, vCelular);

            cargarRecibidos();
        }
    }
}