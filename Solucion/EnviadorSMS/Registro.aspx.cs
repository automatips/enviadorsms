﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Modelo;
using Persistencia;

namespace EnviadorSMS
{
    public partial class Registro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cargarLocalidades();
            }
        }

        protected void cargarLocalidades()
        {
            ddlLocalidad.Items.Add("EL BOLSON (PROV. RIO NEGRO)");
            ddlLocalidad.Items.Add("CANDELARIA (PROV.  SAN LUIS)");
            ddlLocalidad.Items.Add("USHUAIA");
            ddlLocalidad.Items.Add("CASILDA");
            ddlLocalidad.Items.Add("GOBERNADOR COSTA");
            ddlLocalidad.Items.Add("LAS LAJITAS");
            ddlLocalidad.Items.Add("LAS TOSCAS (PROV. SANTA FE)");
            ddlLocalidad.Items.Add("SAN FRANCISCO DEL MONTE DE ORO");
            ddlLocalidad.Items.Add("CASTELLI");
            ddlLocalidad.Items.Add("SAN FRANCISCO (PROV. CORDOBA)");
            ddlLocalidad.Items.Add("LA PUERTA (PROV.  CORDOBA)");
            ddlLocalidad.Items.Add("BERNARDO DE IRIGOYEN");
            ddlLocalidad.Items.Add("AMERICA");
            ddlLocalidad.Items.Add("SALADILLO (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("ROJAS");
            ddlLocalidad.Items.Add("SAN CRISTOBAL");
            ddlLocalidad.Items.Add("AÑATUYA");
            ddlLocalidad.Items.Add("RUFINO");
            ddlLocalidad.Items.Add("GENERAL PIRAN");
            ddlLocalidad.Items.Add("RIO CUARTO");
            ddlLocalidad.Items.Add("SAN PEDRO (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("ALEJANDRO KORN");
            ddlLocalidad.Items.Add("COLON (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("ALTO RIO SENGUER");
            ddlLocalidad.Items.Add("PERGAMINO");
            ddlLocalidad.Items.Add("CARHUE");
            ddlLocalidad.Items.Add("EL CALAFATE");
            ddlLocalidad.Items.Add("MORTEROS");
            ddlLocalidad.Items.Add("VALCHETA");
            ddlLocalidad.Items.Add("ÑORQUINCO");
            ddlLocalidad.Items.Add("CAÑUELAS");
            ddlLocalidad.Items.Add("CANDELARIA (PROV.  MISIONES)");
            ddlLocalidad.Items.Add("SAN CLEMENTE DEL TUYU");
            ddlLocalidad.Items.Add("CORDOBA");
            ddlLocalidad.Items.Add("OLAVARRIA");
            ddlLocalidad.Items.Add("ARROYO SECO");
            ddlLocalidad.Items.Add("SUNCHALES");
            ddlLocalidad.Items.Add("PARANA");
            ddlLocalidad.Items.Add("RAFAELA");
            ddlLocalidad.Items.Add("MONTEAGUDO");
            ddlLocalidad.Items.Add("ALCORTA");
            ddlLocalidad.Items.Add("PASO DE LA PATRIA");
            ddlLocalidad.Items.Add("SALAZAR");
            ddlLocalidad.Items.Add("BOLIVAR");
            ddlLocalidad.Items.Add("JUAN MARIA GUTIERREZ");
            ddlLocalidad.Items.Add("PUERTO RICO");
            ddlLocalidad.Items.Add("GENERAL BELGRANO");
            ddlLocalidad.Items.Add("JUAN B. ALBERDI");
            ddlLocalidad.Items.Add("GENERAL PINTO");
            ddlLocalidad.Items.Add("SAN MARTIN (PROV.  MENDOZA)");
            ddlLocalidad.Items.Add("VICTORIA");
            ddlLocalidad.Items.Add("MAKALLE");
            ddlLocalidad.Items.Add("GENERAL CONESA (PROV.  RIO NEGRO)");
            ddlLocalidad.Items.Add("ARROYITO (PROV.  CORDOBA)");
            ddlLocalidad.Items.Add("NORBERTO DE LA RIESTRA");
            ddlLocalidad.Items.Add("SAN ANTONIO OESTE");
            ddlLocalidad.Items.Add("MONTE QUEMADO");
            ddlLocalidad.Items.Add("CALETA OLIVIA");
            ddlLocalidad.Items.Add("BRAGADO");
            ddlLocalidad.Items.Add("ROSARIO DEL TALA");
            ddlLocalidad.Items.Add("ESPERANZA (PROV.  SANTA FE)");
            ddlLocalidad.Items.Add("QUEMU QUEMU");
            ddlLocalidad.Items.Add("RECREO (PROV.  CATAMARCA)");
            ddlLocalidad.Items.Add("SAN LORENZO (PROV.  SANTA FE)");
            ddlLocalidad.Items.Add("NOGOYA");
            ddlLocalidad.Items.Add("GENERAL PICO");
            ddlLocalidad.Items.Add("CATRIEL");
            ddlLocalidad.Items.Add("MERCEDES (PROV.  CORRIENTES)");
            ddlLocalidad.Items.Add("RESISTENCIA");
            ddlLocalidad.Items.Add("GENERAL LAMADRID");
            ddlLocalidad.Items.Add("TRES ARROYOS");
            ddlLocalidad.Items.Add("RIO PRIMERO");
            ddlLocalidad.Items.Add("28 DE NOVIEMBRE");
            ddlLocalidad.Items.Add("DARREGUEIRA");
            ddlLocalidad.Items.Add("ASCENCION");
            ddlLocalidad.Items.Add("SAN FRANCISCO DEL CHAÑAR");
            ddlLocalidad.Items.Add("VICUÑA MACKENNA");
            ddlLocalidad.Items.Add("ANDALGALA");
            ddlLocalidad.Items.Add("GOBERNADOR CRESPO");
            ddlLocalidad.Items.Add("SALTA");
            ddlLocalidad.Items.Add("CUTRAL CO");
            ddlLocalidad.Items.Add("RAMALLO");
            ddlLocalidad.Items.Add("GENERAL MOSCONI");
            ddlLocalidad.Items.Add("CAMPO GALLO");
            ddlLocalidad.Items.Add("LA PLATA");
            ddlLocalidad.Items.Add("CHARATA");
            ddlLocalidad.Items.Add("SANTA MARIA (PROV.  CATAMARCA)");
            ddlLocalidad.Items.Add("NONOGASTA");
            ddlLocalidad.Items.Add("SAN LUIS DEL PALMAR");
            ddlLocalidad.Items.Add("GENERAL ARENALES");
            ddlLocalidad.Items.Add("CARLOS TEJEDOR");
            ddlLocalidad.Items.Add("COLAN CONHUE (PROV.  CHUBUT)");
            ddlLocalidad.Items.Add("LUJAN DE CUYO");
            ddlLocalidad.Items.Add("CAMPANA");
            ddlLocalidad.Items.Add("RIO MAYO");
            ddlLocalidad.Items.Add("CHOELE CHOEL");
            ddlLocalidad.Items.Add("GENERAL ALVEAR (PROV.  MENDOZA)");
            ddlLocalidad.Items.Add("SAN CARLOS DE BARILOCHE");
            ddlLocalidad.Items.Add("REALICO");
            ddlLocalidad.Items.Add("ALCARAZ");
            ddlLocalidad.Items.Add("VILLA LA ANGOSTURA");
            ddlLocalidad.Items.Add("SAN JAVIER (PROV.  SANTA FE)");
            ddlLocalidad.Items.Add("CORONEL VIDAL");
            ddlLocalidad.Items.Add("GUAMINI");
            ddlLocalidad.Items.Add("ROSARIO");
            ddlLocalidad.Items.Add("PUERTO SANTA CRUZ");
            ddlLocalidad.Items.Add("SAN CARLOS CENTRO");
            ddlLocalidad.Items.Add("CAPITAN SARMIENTO");
            ddlLocalidad.Items.Add("CHASCOMUS");
            ddlLocalidad.Items.Add("MENCUE");
            ddlLocalidad.Items.Add("TRANSITO");
            ddlLocalidad.Items.Add("COLONIA 25 DE MAYO");
            ddlLocalidad.Items.Add("PUERTO IGUAZU");
            ddlLocalidad.Items.Add("SUNCHO CORRAL");
            ddlLocalidad.Items.Add("SAN JAVIER (PROV.  MISIONES)");
            ddlLocalidad.Items.Add("LONCOPUE");
            ddlLocalidad.Items.Add("NUEVA ESPERANZA");
            ddlLocalidad.Items.Add("QUIROGA");
            ddlLocalidad.Items.Add("MACHAGAI");
            ddlLocalidad.Items.Add("LAGUNA BRAVA");
            ddlLocalidad.Items.Add("GUALEGUAY");
            ddlLocalidad.Items.Add("ACEBAL");
            ddlLocalidad.Items.Add("VENADO TUERTO");
            ddlLocalidad.Items.Add("PILAR (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("LA DULCE");
            ddlLocalidad.Items.Add("ESCOBAR");
            ddlLocalidad.Items.Add("LA PAZ (PROV.  MENDOZA)");
            ddlLocalidad.Items.Add("FRAGA");
            ddlLocalidad.Items.Add("BAHIA BLANCA");
            ddlLocalidad.Items.Add("SANTA ROSA (PROV.  LA PAMPA)");
            ddlLocalidad.Items.Add("TERMAS DE RIO HONDO");
            ddlLocalidad.Items.Add("SIERRA GRANDE");
            ddlLocalidad.Items.Add("GENERAL ROCA (PROV.  RIO NEGRO)");
            ddlLocalidad.Items.Add("ARRIBEÑOS");
            ddlLocalidad.Items.Add("COMANDANTE LUIS PIEDRA BUENA");
            ddlLocalidad.Items.Add("MINISTRO RAMOS MEXIA");
            ddlLocalidad.Items.Add("MARCOS PAZ");
            ddlLocalidad.Items.Add("TRELEW");
            ddlLocalidad.Items.Add("ESQUEL");
            ddlLocalidad.Items.Add("GENERAL CONESA (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("BALNEARIA");
            ddlLocalidad.Items.Add("CHACHARRAMENDI");
            ddlLocalidad.Items.Add("SALADAS");
            ddlLocalidad.Items.Add("RAFAEL OBLIGADO");
            ddlLocalidad.Items.Add("SANTA FE");
            ddlLocalidad.Items.Add("CONA NIYEU");
            ddlLocalidad.Items.Add("ALUMINE");
            ddlLocalidad.Items.Add("SARMIENTO (PROV.  CHUBUT)");
            ddlLocalidad.Items.Add("LINCOLN");
            ddlLocalidad.Items.Add("JOAQUIN V. GONZALEZ");
            ddlLocalidad.Items.Add("BARRIO LOS PESCADORES");
            ddlLocalidad.Items.Add("SAN RAFAEL (PROV. MENDOZA)");
            ddlLocalidad.Items.Add("TARTAGAL (PROV.  SALTA)");
            ddlLocalidad.Items.Add("VILLA DOLORES");
            ddlLocalidad.Items.Add("VILLA DEL ROSARIO (PROV.  CORDOBA)");
            ddlLocalidad.Items.Add("MARCOS JUAREZ");
            ddlLocalidad.Items.Add("PASO DE LOS LIBRES");
            ddlLocalidad.Items.Add("TRANCAS");
            ddlLocalidad.Items.Add("PILA");
            ddlLocalidad.Items.Add("RIO GALLEGOS");
            ddlLocalidad.Items.Add("RINCON DE LOS SAUCES");
            ddlLocalidad.Items.Add("RIACHUELO");
            ddlLocalidad.Items.Add("SAN JORGE (PROV.  SANTA FE)");
            ddlLocalidad.Items.Add("MONTEROS");
            ddlLocalidad.Items.Add("EMBAJADOR MARTINI");
            ddlLocalidad.Items.Add("VILLA MASCARDI");
            ddlLocalidad.Items.Add("PUERTO DESEADO");
            ddlLocalidad.Items.Add("SAN ANTONIO DE ARECO");
            ddlLocalidad.Items.Add("DEAN FUNES");
            ddlLocalidad.Items.Add("SANTO TOME");
            ddlLocalidad.Items.Add("CATAMARCA");
            ddlLocalidad.Items.Add("GENERAL MADARIAGA");
            ddlLocalidad.Items.Add("LABOULAYE");
            ddlLocalidad.Items.Add("CONCEPCION DEL URUGUAY");
            ddlLocalidad.Items.Add("NASCHEL");
            ddlLocalidad.Items.Add("SAN JUSTO (PROV.  SANTA FE)");
            ddlLocalidad.Items.Add("BELL VILLE");
            ddlLocalidad.Items.Add("HUANGUELEN SUR");
            ddlLocalidad.Items.Add("CARILO");
            ddlLocalidad.Items.Add("AIMOGASTA");
            ddlLocalidad.Items.Add("SAN NICOLAS");
            ddlLocalidad.Items.Add("MAIPU (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("PUNTA ALTA");
            ddlLocalidad.Items.Add("MERCEDES (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("EL CUY");
            ddlLocalidad.Items.Add("VEDIA");
            ddlLocalidad.Items.Add("SAN MANUEL (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("INGENIERO LUIGGI");
            ddlLocalidad.Items.Add("CONCORDIA");
            ddlLocalidad.Items.Add("MAIPU (PROV.  MENDOZA)");
            ddlLocalidad.Items.Add("COLON (PROV.  ENTRE RIOS)");
            ddlLocalidad.Items.Add("ARGÜELLO");
            ddlLocalidad.Items.Add("JESUS MARIA");
            ddlLocalidad.Items.Add("ADELIA MARIA");
            ddlLocalidad.Items.Add("GENERAL ALVEAR (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("MAR DE AJO");
            ddlLocalidad.Items.Add("JUNIN (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("INGENIERO JACOBACCI");
            ddlLocalidad.Items.Add("GONZALEZ CATAN");
            ddlLocalidad.Items.Add("PUELCHES");
            ddlLocalidad.Items.Add("SAN PATRICIO DEL CHAÑAR");
            ddlLocalidad.Items.Add("MERCEDES (PROV.  SAN LUIS)");
            ddlLocalidad.Items.Add("RECONQUISTA");
            ddlLocalidad.Items.Add("ITUZAINGO");
            ddlLocalidad.Items.Add("WINIFREDA");
            ddlLocalidad.Items.Add("MONTE HERMOSO");
            ddlLocalidad.Items.Add("NECOCHEA");
            ddlLocalidad.Items.Add("TAPALQUE");
            ddlLocalidad.Items.Add("CORONEL PRINGLES");
            ddlLocalidad.Items.Add("JOSE C. PAZ");
            ddlLocalidad.Items.Add("LAS FLORES (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("CHEPES");
            ddlLocalidad.Items.Add("MACACHIN");
            ddlLocalidad.Items.Add("LA REFORMA");
            ddlLocalidad.Items.Add("LAGO PUELO");
            ddlLocalidad.Items.Add("OLIVA");
            ddlLocalidad.Items.Add("CORONEL BRANDSEN");
            ddlLocalidad.Items.Add("CURUZU CUATIA");
            ddlLocalidad.Items.Add("FORMOSA");
            ddlLocalidad.Items.Add("CHIVILCOY");
            ddlLocalidad.Items.Add("PUERTO TIROL");
            ddlLocalidad.Items.Add("GARUPA");
            ddlLocalidad.Items.Add("CORONEL SUAREZ");
            ddlLocalidad.Items.Add("VILLA IRIS");
            ddlLocalidad.Items.Add("O BRIEN");
            ddlLocalidad.Items.Add("LLAMBI CAMPBELL");
            ddlLocalidad.Items.Add("LOPEZ CAMELO");
            ddlLocalidad.Items.Add("VILLA ANGELA");
            ddlLocalidad.Items.Add("VILLA DE MARIA DE RIO SECO");
            ddlLocalidad.Items.Add("RIO COLORADO (PROV. RIO NEGRO)");
            ddlLocalidad.Items.Add("ANILLACO");
            ddlLocalidad.Items.Add("PRESIDENCIA ROQUE SAENZ PEÑA");
            ddlLocalidad.Items.Add("RAPELLI");
            ddlLocalidad.Items.Add("CLAROMECO");
            ddlLocalidad.Items.Add("VILLA REGINA");
            ddlLocalidad.Items.Add("JUNIN DE LOS ANDES");
            ddlLocalidad.Items.Add("CHACABUCO");
            ddlLocalidad.Items.Add("BARRANCAS (PROV.  SANTA FE)");
            ddlLocalidad.Items.Add("MONTE CASEROS");
            ddlLocalidad.Items.Add("INGENIERO GUILLERMO N. JUAREZ");
            ddlLocalidad.Items.Add("9 DE JULIO (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("MERLO (PROV.  SAN LUIS)");
            ddlLocalidad.Items.Add("IBARRETA");
            ddlLocalidad.Items.Add("COMANDANTE FONTANA");
            ddlLocalidad.Items.Add("CAFAYATE");
            ddlLocalidad.Items.Add("LEZAMA");
            ddlLocalidad.Items.Add("PEHUAJO");
            ddlLocalidad.Items.Add("SALTO");
            ddlLocalidad.Items.Add("RIO SEGUNDO");
            ddlLocalidad.Items.Add("CORONEL MOLDES (PROV.  CORDOBA)");
            ddlLocalidad.Items.Add("OJO DE AGUA (PROV. SANTIAGO DEL ESTERO)");
            ddlLocalidad.Items.Add("AMAICHA DEL VALLE");
            ddlLocalidad.Items.Add("MEDANOS (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("25 DE MAYO (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("TECKA");
            ddlLocalidad.Items.Add("CORONEL DORREGO");
            ddlLocalidad.Items.Add("LA PAZ (PROV.  ENTRE RIOS)");
            ddlLocalidad.Items.Add("DAIREAUX");
            ddlLocalidad.Items.Add("ZARATE");
            ddlLocalidad.Items.Add("SANTA TERESITA");
            ddlLocalidad.Items.Add("TINTINA");
            ddlLocalidad.Items.Add("CHOS MALAL");
            ddlLocalidad.Items.Add("MAR DEL PLATA");
            ddlLocalidad.Items.Add("LUJAN (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("USPALLATA");
            ddlLocalidad.Items.Add("GUERNICA");
            ddlLocalidad.Items.Add("GUALEGUAYCHU");
            ddlLocalidad.Items.Add("TRES LOMAS");
            ddlLocalidad.Items.Add("PERITO MORENO");
            ddlLocalidad.Items.Add("ARRUFO");
            ddlLocalidad.Items.Add("CANALS");
            ddlLocalidad.Items.Add("NAVARRO");
            ddlLocalidad.Items.Add("MOJON DE FIERRO");
            ddlLocalidad.Items.Add("SAN SALVADOR DE JUJUY");
            ddlLocalidad.Items.Add("LAMADRID");
            ddlLocalidad.Items.Add("CHAMICAL");
            ddlLocalidad.Items.Add("PUELEN");
            ddlLocalidad.Items.Add("BAHIA BUSTAMANTE");
            ddlLocalidad.Items.Add("GALVEZ");
            ddlLocalidad.Items.Add("MARGARITA BELEN");
            ddlLocalidad.Items.Add("AZUL");
            ddlLocalidad.Items.Add("BURRUYACU");
            ddlLocalidad.Items.Add("GOYA");
            ddlLocalidad.Items.Add("BANDERA");
            ddlLocalidad.Items.Add("LIBERTADOR GENERAL SAN MARTIN");
            ddlLocalidad.Items.Add("LA TOMA");
            ddlLocalidad.Items.Add("PUERTO BASTIANI");
            ddlLocalidad.Items.Add("SALLIQUELO");
            ddlLocalidad.Items.Add("SAN ANDRES DE GILES");
            ddlLocalidad.Items.Add("MENDOZA");
            ddlLocalidad.Items.Add("TAFI DEL VALLE");
            ddlLocalidad.Items.Add("ALTA GRACIA");
            ddlLocalidad.Items.Add("LA QUIACA");
            ddlLocalidad.Items.Add("MALARGÜE");
            ddlLocalidad.Items.Add("EL MAITEN");
            ddlLocalidad.Items.Add("QUIMILI");
            ddlLocalidad.Items.Add("VILLA SAN MARTIN");
            ddlLocalidad.Items.Add("BALCARCE");
            ddlLocalidad.Items.Add("MONTE");
            ddlLocalidad.Items.Add("CAMARONES");
            ddlLocalidad.Items.Add("POSADAS");
            ddlLocalidad.Items.Add("CORRIENTES");
            ddlLocalidad.Items.Add("EL SOMBRERO");
            ddlLocalidad.Items.Add("VIEDMA");
            ddlLocalidad.Items.Add("TUNUYAN");
            ddlLocalidad.Items.Add("DUDIGNAC");
            ddlLocalidad.Items.Add("CAVIAHUE");
            ddlLocalidad.Items.Add("LAPRIDA");
            ddlLocalidad.Items.Add("TINOGASTA");
            ddlLocalidad.Items.Add("APOSTOLES");
            ddlLocalidad.Items.Add("VILLAGUAY");
            ddlLocalidad.Items.Add("AGUILARES");
            ddlLocalidad.Items.Add("MAGDALENA");
            ddlLocalidad.Items.Add("SAN AGUSTIN DEL VALLE FERTIL");
            ddlLocalidad.Items.Add("ELDORADO");
            ddlLocalidad.Items.Add("LOBERIA");
            ddlLocalidad.Items.Add("ORENSE");
            ddlLocalidad.Items.Add("COMODORO RIVADAVIA");
            ddlLocalidad.Items.Add("LOS TOLDOS (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("PEDRO LURO");
            ddlLocalidad.Items.Add("GENERAL VILLEGAS");
            ddlLocalidad.Items.Add("LAS LOMITAS");
            ddlLocalidad.Items.Add("PIGÜE");
            ddlLocalidad.Items.Add("MIRAMAR (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("BOUCHARD");
            ddlLocalidad.Items.Add("TILISARAO");
            ddlLocalidad.Items.Add("COLONIA BARON");
            ddlLocalidad.Items.Add("ZAPALA");
            ddlLocalidad.Items.Add("CARMEN DE ARECO");
            ddlLocalidad.Items.Add("BOVRIL");
            ddlLocalidad.Items.Add("CARLOS CASARES");
            ddlLocalidad.Items.Add("BUENA ESPERANZA");
            ddlLocalidad.Items.Add("TRENQUE LAUQUEN");
            ddlLocalidad.Items.Add("VICTORICA");
            ddlLocalidad.Items.Add("SIERRA COLORADA");
            ddlLocalidad.Items.Add("MERLO (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("MORENO");
            ddlLocalidad.Items.Add("JUAN N. FERNANDEZ");
            ddlLocalidad.Items.Add("EDUARDO CASTEX");
            ddlLocalidad.Items.Add("RANCHILLOS");
            ddlLocalidad.Items.Add("BUTA RANQUIL");
            ddlLocalidad.Items.Add("PRESIDENCIA DE LA PLAZA");
            ddlLocalidad.Items.Add("VILLA MARIA");
            ddlLocalidad.Items.Add("QUINES");
            ddlLocalidad.Items.Add("HUMAHUACA");
            ddlLocalidad.Items.Add("BENITO JUAREZ");
            ddlLocalidad.Items.Add("ARRECIFES");
            ddlLocalidad.Items.Add("CASBAS");
            ddlLocalidad.Items.Add("FIRMAT");
            ddlLocalidad.Items.Add("VILLA CONSTITUCION");
            ddlLocalidad.Items.Add("NEUQUEN");
            ddlLocalidad.Items.Add("SAN MIGUEL DE TUCUMAN");
            ddlLocalidad.Items.Add("SAN JULIAN");
            ddlLocalidad.Items.Add("VILLA DEL TOTORAL");
            ddlLocalidad.Items.Add("GAN GAN");
            ddlLocalidad.Items.Add("AMBA");
            ddlLocalidad.Items.Add("GASTRE");
            ddlLocalidad.Items.Add("SUMAMPA");
            ddlLocalidad.Items.Add("SANTA ANA (PROV.  CORRIENTES)");
            ddlLocalidad.Items.Add("LAS VARILLAS");
            ddlLocalidad.Items.Add("SANTA ROSA DE CALAMUCHITA");
            ddlLocalidad.Items.Add("SALSACATE");
            ddlLocalidad.Items.Add("CHAJARI");
            ddlLocalidad.Items.Add("CHILECITO");
            ddlLocalidad.Items.Add("LEANDRO N. ALEM (PROV.  MISIONES)");
            ddlLocalidad.Items.Add("DOLORES (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("GENERAL JOSE DE SAN MARTIN");
            ddlLocalidad.Items.Add("VILLA HUIDOBRO");
            ddlLocalidad.Items.Add("METAN");
            ddlLocalidad.Items.Add("COMANDANTE N. OTAMENDI");
            ddlLocalidad.Items.Add("SAN MARTIN DE LOS ANDES");
            ddlLocalidad.Items.Add("CONCEPCION (PROV. TUCUMAN)");
            ddlLocalidad.Items.Add("BARREAL");
            ddlLocalidad.Items.Add("SAN JOSE DE FELICIANO");
            ddlLocalidad.Items.Add("CAA CATI");
            ddlLocalidad.Items.Add("VILLALONGA");
            ddlLocalidad.Items.Add("PASO DE INDIOS");
            ddlLocalidad.Items.Add("CALINGASTA");
            ddlLocalidad.Items.Add("RIVERA");
            ddlLocalidad.Items.Add("BERNASCONI");
            ddlLocalidad.Items.Add("MOISES VILLE");
            ddlLocalidad.Items.Add("EL TREBOL");
            ddlLocalidad.Items.Add("PINAMAR");
            ddlLocalidad.Items.Add("TANDIL");
            ddlLocalidad.Items.Add("AYACUCHO");
            ddlLocalidad.Items.Add("RAUCH");
            ddlLocalidad.Items.Add("CALEUFU");
            ddlLocalidad.Items.Add("LOBOS");
            ddlLocalidad.Items.Add("JACHAL");
            ddlLocalidad.Items.Add("RIO TERCERO");
            ddlLocalidad.Items.Add("CRUZ ALTA");
            ddlLocalidad.Items.Add("PLAYA CHAPADMALAL");
            ddlLocalidad.Items.Add("VILLA CARLOS PAZ");
            ddlLocalidad.Items.Add("ORAN");
            ddlLocalidad.Items.Add("INRIVILLE");
            ddlLocalidad.Items.Add("FORTIN OLAVARRIA");
            ddlLocalidad.Items.Add("FEDERAL");
            ddlLocalidad.Items.Add("RIO GRANDE (PROV.  TIERRA DEL FUEGO)");
            ddlLocalidad.Items.Add("CAPILLA DEL SEÑOR");
            ddlLocalidad.Items.Add("SAN LUIS (PROV. SAN LUIS)");
            ddlLocalidad.Items.Add("SAN PEDRO (PROV.  JUJUY)");
            ddlLocalidad.Items.Add("CERES");
            ddlLocalidad.Items.Add("PICUN LEUFU");
            ddlLocalidad.Items.Add("COLONIA BENITEZ");
            ddlLocalidad.Items.Add("GLEW");
            ddlLocalidad.Items.Add("CRUZ DEL EJE");
            ddlLocalidad.Items.Add("CARLOS SPEGAZZINI");
            ddlLocalidad.Items.Add("SANTA TERESA (PROV.  SANTA FE)");
            ddlLocalidad.Items.Add("GENERAL ACHA");
            ddlLocalidad.Items.Add("VERA");
            ddlLocalidad.Items.Add("CAÑADA DE GOMEZ");
            ddlLocalidad.Items.Add("CORRAL DE BUSTOS");
            ddlLocalidad.Items.Add("LA RIOJA");
            ddlLocalidad.Items.Add("VILLA GESELL");
            ddlLocalidad.Items.Add("ANDACOLLO");
            ddlLocalidad.Items.Add("CUCHILLO CO");
            ddlLocalidad.Items.Add("ISLA DEL CERRITO");
            ddlLocalidad.Items.Add("PUERTO MADRYN");
            ddlLocalidad.Items.Add("COMALLO");
            ddlLocalidad.Items.Add("CHARADAI");
            ddlLocalidad.Items.Add("SANTA CLARA DEL MAR");
            ddlLocalidad.Items.Add("RIO TURBIO");
            ddlLocalidad.Items.Add("SAMPACHO");
            ddlLocalidad.Items.Add("SAN BERNARDO (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("OBERA");
            ddlLocalidad.Items.Add("SAN JUAN");
            ddlLocalidad.Items.Add("SANTIAGO DEL ESTERO");
            ddlLocalidad.Items.Add("VERONICA");
            ddlLocalidad.Items.Add("LA FALDA");
            ddlLocalidad.Items.Add("LA CARLOTA");
            ddlLocalidad.Items.Add("FRIAS");
            ddlLocalidad.Items.Add("HUINCA RENANCO");
            ddlLocalidad.Items.Add("GUATRACHE");
            ddlLocalidad.Items.Add("PIEDRA DEL AGUILA");
            ddlLocalidad.Items.Add("RAWSON (PROV.  CHUBUT)");
            ddlLocalidad.Items.Add("SAN VICENTE (PROV.  BUENOS AIRES)");
            ddlLocalidad.Items.Add("CLORINDA");
        }

        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            try
            {
                if (
                    txtUsuario.Text != string.Empty
                    &&
                    txtNombre.Text != string.Empty
                    &&
                    txtPassword.Text != string.Empty
                    &&
                    txtPasswordConfirmacion.Text != string.Empty
                    &&
                    txtCelular.Text != string.Empty
                    &&
                    txtNegocio.Text != string.Empty
                    &&
                    txtPassword.Text == txtPasswordConfirmacion.Text
                    )
                {
                    UCTraza1.InsertarTraza("Registro de usuario");
                    DiscadoDatos discadoDatos = new DiscadoDatos();
                    discadoDatos.RegistroUsuario(
                        txtNombre.Text,
                        txtUsuario.Text,
                        txtPassword.Text,
                        2,
                        txtEmail.Text,
                        txtCelular.Text,
                        ddlLocalidad.SelectedItem.Text
                        );
                    Response.Redirect("Login.aspx");
                }
            }
            catch
            {
                
            }
        }
    }
}