﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Persistencia;
using System.Data;

namespace EnviadorSMS
{
    public partial class RecibidoContactados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarRecibidos();
        }

        protected void cargarRecibidos()
        {
            if (Session["usuIdUsuario"] != null)
            {
                try
                {
                    UCTraza1.InsertarTraza("Obteniendo entregados con IdUsuario: " + Session["usuIdUsuario"].ToString());
                    DiscadoDatos discadoDatos = new DiscadoDatos();
                    DataTable dtRecibidos = discadoDatos.ObtenerRecibidoContactados(Convert.ToInt32(Session["usuIdUsuario"].ToString()));
                    gvSMSRecibidos.DataSource = dtRecibidos;
                    gvSMSRecibidos.DataBind();
                    upGridViewSMSRecibidos.Update();
                }
                catch (Exception)
                {
                    Response.Redirect("Precios.aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected DataTable obtenerEstados()
        {
            DataTable dt = new DataTable();
            DiscadoDatos discadoDatos = new DiscadoDatos();
            dt = discadoDatos.ObtenerRecibidoEstados();
            return dt;
        }

        protected void gvSMSRecibidos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSMSRecibidos.PageIndex = e.NewPageIndex;
                gvSMSRecibidos.DataBind();
                cargarRecibidos();
            }
            catch (Exception)
            {
            }
        }

        protected void ddlEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarRecibidos();
        }
        
    }
}