﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registro.aspx.cs" Inherits="EnviadorSMS.Registro" %>
<%@ Register Src="~/Controls/UCTraza.ascx" TagPrefix="uc" TagName="UCTraza" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Enviador SMS - Registro</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Registre su cuenta en minutos!</h1>
              </div>
              <form id="Form1" class="user" runat="server">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <asp:TextBox ID="txtNombre" CssClass="form-control form-control-user" placeholder="Nombre completo" runat="server" TextMode="SingleLine" />
                  </div>
                  <div class="col-sm-6">
                    <asp:TextBox ID="txtCelular" CssClass="form-control form-control-user" placeholder="Teléfono Celular" runat="server" TextMode="SingleLine" />
                  </div>
                </div>

                <div class="form-group row">
                    <asp:TextBox ID="txtUsuario" CssClass="form-control form-control-user" placeholder="Usuario" runat="server" TextMode="SingleLine" />
                </div>

                <div class="form-group">
                  <asp:TextBox ID="txtEmail" CssClass="form-control form-control-user" aria-describedby="emailHelp" placeholder="ingresesuemail@sucorreo.com" runat="server" TextMode="SingleLine" />
                </div>

                <div class="form-group">
                        <label>
                            <asp:Label Text="Seleccione su localidad: " runat="server" />
                        </label>
                        <asp:DropDownList ID="ddlLocalidad" CssClass="form-control select-single" runat="server" />
                </div>

                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <asp:TextBox ID="txtPassword" CssClass="form-control form-control-user" aria-describedby="emailHelp" placeholder="Ingrese su password..." runat="server" TextMode="Password" />
                  </div>
                  <div class="col-sm-6">
                      <asp:TextBox ID="txtPasswordConfirmacion" CssClass="form-control form-control-user" aria-describedby="emailHelp" placeholder="Confirme su password..." runat="server" TextMode="Password" />
                  </div>
                </div>

                <div class="form-group row">
                    <asp:TextBox ID="txtNegocio" CssClass="form-control form-control-user" placeholder="Describa el tipo de negocio que desea publicitar" runat="server" TextMode="SingleLine" />
                </div>

                <div class="form-group">
                    <asp:Button ID="btnRegistro" 
                        Text="Registrar usuario" CssClass="btn btn-primary" runat="server" OnClick="btnRegistro_Click"/>
                </div>
                <hr>
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="#">¿Olvidó su contraseña?</a>
              </div>
              <div class="text-center">
                <a class="small" href="Login.aspx">¿Ya tiene una cuenta? Ingrese!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
        <uc:UCTraza runat="server" ID="UCTraza1" />
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
