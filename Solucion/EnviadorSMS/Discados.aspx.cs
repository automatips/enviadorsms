﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Persistencia;

namespace EnviadorSMS
{
    public partial class Discados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarDiscados();
        }

        protected void cargarDiscados()
        {
            if (Session["usuIdUsuario"] != null)
            {
                try
                {
                    UCTraza1.InsertarTraza("Obteniendo entregados con IdUsuario: " + Session["usuIdUsuario"].ToString());
                    DiscadoDatos discadoDatos = new DiscadoDatos();
                    gvSMSDiscados.DataSource = discadoDatos.ObtenerDiscados(Convert.ToInt32(Session["usuIdUsuario"].ToString()));
                    gvSMSDiscados.DataBind();
                    upGridViewSMSDiscados.Update();
                }
                catch (Exception)
                {
                    Response.Redirect("Precios.aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void gvSMSDiscados_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSMSDiscados.PageIndex = e.NewPageIndex;
                gvSMSDiscados.DataBind();
                cargarDiscados();
            }
            catch (Exception)
            {
            }
        }
    }
}