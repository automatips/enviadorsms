﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Persistencia;

namespace EnviadorSMS
{
    public partial class Entregados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                cargarEntregados();
            }

        }

        protected void cargarEntregados()
        {
            if (Session["usuIdUsuario"] != null)
            {
                try
                {
                    UCTraza1.InsertarTraza("Obteniendo entregados con IdUsuario: "+ Session["usuIdUsuario"].ToString());
                    DiscadoDatos discadoDatos = new DiscadoDatos();
                    gvSMSEntregados.DataSource = discadoDatos.ObtenerEntregados(Convert.ToInt32(Session["usuIdUsuario"].ToString()));
                    gvSMSEntregados.DataBind();
                    upGridViewSMSEntregados.Update();
                }
                catch (Exception)
                {
                    Response.Redirect("Precios.aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void gvSMSEntregados_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (Session["nivelAdmin"] != null)
            {
                string vEsAdmin = Session["nivelAdmin"].ToString();
                if (vEsAdmin == "true")
                {
                    try
                    {
                        gvSMSEntregados.PageIndex = e.NewPageIndex;
                        gvSMSEntregados.DataBind();
                        cargarEntregados();
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            else
            {
                try
                {
                    gvSMSEntregadosUser.PageIndex = e.NewPageIndex;
                    gvSMSEntregadosUser.DataBind();
                    cargarEntregados();
                }
                catch (Exception)
                {
                }
            }


        }
    }
}